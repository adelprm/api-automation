<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Current Weather Data</name>
   <tag></tag>
   <elementGuidId>057d4060-4636-4ce1-ac41-e472c421bf0d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <connectionTimeout>-1</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <katalonVersion>7.7.2</katalonVersion>
   <maxResponseSize>-1</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>https://api.weatherbit.io/v2.0/current?lat=${a_lat}&amp;lon=${a_lon}&amp;key=${a_key}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>-1</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <variables>
      <defaultValue>'40.730610'</defaultValue>
      <description></description>
      <id>026e3817-655e-41c7-81cb-30a5d5e24094</id>
      <masked>false</masked>
      <name>a_lat</name>
   </variables>
   <variables>
      <defaultValue>'-73.935242'</defaultValue>
      <description></description>
      <id>4e4adc99-2277-46da-b19e-9102a506c18e</id>
      <masked>false</masked>
      <name>a_lon</name>
   </variables>
   <variables>
      <defaultValue>'317b6a58812a48c18b9db736bd98764d'</defaultValue>
      <description></description>
      <id>5d38da87-a5fd-46cf-ad7d-123ffafa24f2</id>
      <masked>false</masked>
      <name>a_key</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
WS.verifyElementPropertyValue(response, 'count', 1)
WS.verifyElementPropertyValue(response, 'data[0].state_code', &quot;NY&quot;)


WS.verifyResponseStatusCode(response, 200)

assertThat(response.getStatusCode()).isEqualTo(200)

</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
